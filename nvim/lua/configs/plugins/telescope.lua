return {
    "nvim-telescope/telescope.nvim",
    branch = "0.1.x",
    dependencies = {
        { "nvim-lua/plenary.nvim" },
        { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
    },
    keys = {
        -- Find
        {
            desc = "Files",
            "<leader>ff",
            function()
                require("telescope.builtin").find_files({ hidden = false, no_ignore = false, prompt_title = "Find files" })
            end,
        },
        {
            desc = "All files",
            "<leader>fF",
            function()
                require("telescope.builtin").find_files({
                    hidden = true,
                    no_ignore = true,
                    prompt_title =
                    "Find all Files"
                })
            end,
        },
        {
            desc = "Git files",
            "<leader>fg",
            function()
                require("telescope.builtin").git_files({ prompt_title = "Find git files" })
            end,
        },
        {
            desc = "Recent files",
            "<leader>fo",
            function()
                require("telescope.builtin").oldfiles({ prompt_title = "Recent files" })
            end,
        },
        -- Search
        {
            desc = "Search",
            "<leader>ss",
            function()
                require("telescope.builtin").live_grep()
            end,
        },
        {
            desc = "Symbol under cursor",
            "<leader>sS",
            function()
                require("telescope.builtin").grep_string()
            end,
        },
        -- Buffers
        {
            desc = "Buffers",
            "<leader>bb",
            function()
                require("telescope.builtin").buffers()
            end,
        },
        -- LSP
        {
            desc = "References",
            "<leader>cr",
            function()
                require("telescope.builtin").lsp_references()
            end,
        },
        {
            desc = "Incoming calls",
            "<leader>ci",
            function()
                require("telescope.builtin").lsp_incoming_calls()
            end,
        },
        {
            desc = "Outgoing calls",
            "<leader>co",
            function()
                require("telescope.builtin").lsp_outgoing_calls()
            end,
        },
        {
            desc = "Symbols",
            "<leader>cs",
            function()
                require("telescope.builtin").lsp_document_symbols()
            end,
        },
        {
            desc = "Diagnostics",
            "<leader>cx",
            function()
                require("telescope.builtin").diagnostics()
            end,
        },
        {
            desc = "Implementation",
            "<leader>cI",
            function()
                require("telescope.builtin").lsp_implementations()
            end,
        },
        {
            desc = "Definition",
            "<leader>cd",
            function()
                require("telescope.builtin").lsp_definitions()
            end,
        },
        {
            desc = "Type definition",
            "<leader>ct",
            function()
                require("telescope.builtin").lsp_type_definitions()
            end,
        },
        {
            desc = "Neovim commands",
            "<leader>?c",
            function()
                require("telescope.builtin").commands()
            end,
        },
        {
            desc = "Neovim help",
            "<leader>?t",
            function()
                require("telescope.builtin").help_tags()
            end,
        },
        {
            desc = "Linux man",
            "<leader>?m",
            function()
                require("telescope.builtin").man_pages()
            end,
        },
        {
            desc = "Keymaps",
            "<leader>?k",
            function()
                require("telescope.builtin").keymaps()
            end,
        },
    },
    config = function()
        local telescope = require("telescope")
        local actions = require("telescope.actions")

        telescope.setup({
            defaults = {
                mappings = {
                    i = {
                        ["<C-j>"] = actions.move_selection_next,
                        ["<C-k>"] = actions.move_selection_previous,
                    },
                },
                layout_strategy = "vertical",
                layout_config = {
                    height = 0.95,
                    width = 0.95,
                    preview_cutoff = 0,
                },
            },
            pickers = {
                find_files = {
                    previewer = true,
                },
                commands = {
                    previewer = true,
                },
                help_tags = {
                    previewer = true,
                },
            },
        })

        require("telescope").load_extension("fzf")
    end,
}
