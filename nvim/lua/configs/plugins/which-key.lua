return {
    "folke/which-key.nvim",
    event = "VeryLazy",
    init = function()
        vim.o.timeout = true
        vim.o.timeoutlen = 300
    end,
    opts = {
        icons = {
            mappings = false, -- no icons
        },
        spec = {
            { "<leader>b",  group = "Buffer" },
            { "<leader>f",  group = "Find" },
            { "<leader>s",  group = "Search" },
            { "<leader>c",  group = "Code" },
            { "<leader>t",  group = "Toggle" },
            { "<leader>x",  group = "System" },
            { "<leader>?",  group = "Help" },
        }
    },
}
