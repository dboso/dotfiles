return {
    'nvim-treesitter/nvim-treesitter',
    config = function()
        require('nvim-treesitter.configs').setup({
            auto_install = true,

            highlight = {
                enable = true,
                use_languagetree = true
            },
            indent = {
                enable = false
            },
            autotag = {
                enable = false
            },
            context_commentstring = {
                enable = true,
                enable_autocmd = false
            },
            refactor = {
                highlight_definitions = {
                    enable = true
                },
                highlight_current_scope = {
                    enable = false
                }
            }
        })
    end,
    build = function()
        require('nvim-treesitter.install').update({ with_sync = true })
    end,

}
