return {
    {
        'neovim/nvim-lspconfig',
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
        },
        config = function()
            vim.api.nvim_create_autocmd("LspAttach", {
                group = vim.api.nvim_create_augroup("lsp-attach", { clear = true }),
                callback = function(event)
                    vim.keymap.set("n", "<leader>xi", ":LspInfo<CR>", { desc = "LspInfo" })
                    vim.keymap.set("n", "<leader>xl", ":LspLog<CR>", { desc = "LspLog" })
                    vim.keymap.set("n", "<leader>xr", ":LspRestart<CR>", { desc = "LspRestart" })
                    vim.keymap.set("n", "<leader>cR", vim.lsp.buf.rename, { buffer = event.buf, desc = "Rename"})
                    vim.keymap.set({ "n", "x" }, "<leader>ca", vim.lsp.buf.code_action, { buffer = event.buf, desc = "Action"})
                end
            })

            local capabilities = vim.lsp.protocol.make_client_capabilities()
            capabilities = vim.tbl_deep_extend("force", capabilities, require("cmp_nvim_lsp").default_capabilities())

            local lspconfig = require('lspconfig')
            lspconfig.ts_ls.setup({
                capabilities = capabilities
            })
            lspconfig.eslint.setup({
                capabilities = capabilities
            })
            lspconfig.rust_analyzer.setup({
                on_attach = function(client, bufnr)
                    vim.lsp.inlay_hint.enable(false, { bufnr = bufnr })
                end,
                settings = {
                    ["rust-analyzer"] = {
                        imports = {
                            granularity = {
                                group = "module",
                            },
                            prefix = "self",
                        },
                        cargo = {
                            buildScripts = {
                                enable = true,
                            },
                        },
                        procMacro = {
                            enable = true
                        },
                        checkOnSave = {
                            command = "clippy",
                        },
                    }
                }
            })
        end
    },
    {
        "hrsh7th/nvim-cmp",
        event = "InsertEnter",
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            'hrsh7th/cmp-vsnip',
            'hrsh7th/vim-vsnip',
            "rafamadriz/friendly-snippets",
            'neovim/nvim-lspconfig',
        },
        config = function()
            local cmp = require("cmp")

            cmp.setup({
                snippet = {
                    expand = function(args)
                        vim.snippet.expand(args.body)
                    end,
                },

                mapping = cmp.mapping.preset.insert({
                    ["<C-j>"] = cmp.mapping.select_next_item(),
                    ["<C-k>"] = cmp.mapping.select_prev_item(),
                    ["<C-u>"] = cmp.mapping.scroll_docs(-4),
                    ["<C-d>"] = cmp.mapping.scroll_docs(4),
                    ["<C-y>"] = cmp.mapping.confirm({ select = true }),
                    ["<C-Space>"] = cmp.mapping.complete({}),
                    -- ['<C-l>'] = cmp.mapping(function()
                        --     if vim.snippet.expand_or_locally_jumpable() then
                        --         vim.snippet.expand_or_jump()
                        --     end
                        -- end, { 'i', 's' }),
                        -- ['<C-h>'] = cmp.mapping(function()
                            --     if vsnip.locally_jumpable(-1) then
                            --         vsnip.jump(-1)
                            --     end
                            -- end, { 'i', 's' }),
                        }),
                        sources = {
                            { name = "nvim_lsp" },
                            { name = 'vsnip' },
                        },
                    })
                end,
            },
        }
