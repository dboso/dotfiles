-- Leader
vim.g.mapleader = " "
vim.g.localmapleader = " "

-- <ESC> alternative
vim.keymap.set("i", "jj", "<ESC>")

-- Buffers
--- New buffer
vim.keymap.set("n", "<leader>bn", ":enew<CR>", { desc = "New buffer", silent = true })
--- Delete buffer
vim.keymap.set("n", "<leader>bd", ":bd<CR>", { desc = "Delete buffer", silent = true })
-- Save buffer
vim.keymap.set("n", "<leader>bs", ":w<CR>", { desc = "Save buffer" })
--- Last buffer
vim.keymap.set("n", "<leader>bl", "<C-^>", { desc = "Last buffer" })

-- Toggle
--- Search highlight
vim.keymap.set("n", "<leader>ts", ":nohlsearch<CR>", { desc = "Search highlight", silent = true })
--- Line numbers
vim.keymap.set("n", "<leader>tl", require("functions").toggle_lines, { desc = "Line numbers" })
--- Spelling
vim.keymap.set("n", "<leader>tS", require("functions").toggle_spelling, { desc = "Spelling" })
--- Scroll bind
vim.keymap.set("n", "<leader>tb", require("functions").toggle_scrollbind, { desc = "Scrollbind" })
--- Diagnostics list
vim.keymap.set('n', '<leader>tQ', require("functions").toggle_diag_list, { desc = 'Diagnostics list' })
--- Diagnostics for line
vim.keymap.set('n', '<leader>tq', require("functions").toggle_diag_line, { desc = 'Diagnostics for line' })
--- Color Column
vim.keymap.set('n', '<leader>tc', require("functions").toggle_colorcolumn, { desc = 'Color column' })

-- System
vim.keymap.set("n", "<leader>xh", "zw", { desc = "Checkhealth" })
