--- UI
vim.opt.laststatus = 3       -- only the last window will always have a status line
vim.opt.lazyredraw = true    -- don"t update the display while executing macros
vim.opt.cmdheight = 0        -- more space in the neovim command line for displaying messages
vim.opt.signcolumn = 'yes:1' -- always draw 'signcolumn' with fixed width of 1
vim.opt.wrap = false         -- No text wrap
vim.opt.list = true          -- display invisible chars (e.g. trailing whitespace, tabs)
vim.opt.splitright = true    -- open new vsplit to the right
vim.opt.splitbelow = true    -- open new split below
vim.opt.scrolloff = 8        -- when to scroll up/down
vim.opt.sidescrolloff = 8    -- when to scroll left/right
vim.opt.confirm = true       -- confirm dialog of unsaved changes

--- Indentation
local indent = 4
vim.opt.autoindent = true    -- auto indentation
vim.opt.expandtab = true     -- convert tabs to spaces
vim.opt.shiftwidth = indent  -- the number of spaces inserted for each indentation
vim.opt.smartindent = true   -- make indenting smarter
vim.opt.softtabstop = indent -- when hitting <BS>, pretend like a tab is removed, even if spaces
vim.opt.tabstop = indent     -- insert 2 spaces for a tab
vim.opt.shiftround = true    -- use multiple of shiftwidth when indenting with "<" and ">"

--- Search
vim.opt.ignorecase = true    -- ignore case in search patterns
vim.opt.smartcase = true     -- Override 'ignorecase' if search pattern contains upper case characters

--- Backups
vim.opt.backup = false       -- create a backup file
vim.opt.swapfile = false     -- creates a swapfile
vim.opt.writebackup = false  -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited

--- Performace
vim.opt.updatetime = 300     -- signify default updatetime 4000ms is not good for async update
vim.opt.timeoutlen = 250     -- time to wait for a mapped sequence to complete (in milliseconds)
