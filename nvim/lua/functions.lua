local lib = {}

function lib.toggle_spelling()
  vim.o.spell = not vim.o.spell
end

function lib.toggle_lines()
  vim.o.number = not vim.o.number
  vim.o.relativenumber = not vim.o.relativenumber
end

function lib.toggle_scrollbind()
  vim.o.scrollbind = not vim.o.scrollbind
end

function lib.toggle_diag_list()
  local winid = vim.fn.getloclist(0, { winid = 0 }).winid
  if winid == 0 then
    vim.diagnostic.setloclist({})
  else
    vim.cmd.lclose()
  end
end

function lib.toggle_diag_line()
  local found_float = false
  for _, win in ipairs(vim.api.nvim_list_wins()) do
    if vim.api.nvim_win_get_config(win).relative ~= '' then
      vim.api.nvim_win_close(win, true)
      found_float = true
    end
  end
  if found_float then
    return
  end
  vim.diagnostic.open_float(nil, { focus = false })
end

function lib.toggle_colorcolumn()
  if vim.o.colorcolumn > "0" then
    vim.o.colorcolumn = "0"
  else
    vim.o.colorcolumn = "80"
  end
end

return lib
